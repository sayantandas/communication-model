package com.bricknbolt.garuda.communicationModel.Request;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class ReceiverData {
    // conversation data
    @NotNull
    private String serviceRequestId;
    @NotNull
    private String providerConversationId;
}
