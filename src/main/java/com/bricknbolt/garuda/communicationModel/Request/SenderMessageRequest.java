package com.bricknbolt.garuda.communicationModel.Request;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;

import java.util.List;

public interface SenderMessageRequest {
    List<String> getTo();

    void setTo(List<String> to);

    MessageBody getMessage();

    void setMessage(MessageBody message);
}
