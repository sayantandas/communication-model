package com.bricknbolt.garuda.communicationModel.Request;

import com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.MessageBodyType;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class MessageRequest {
    // conversation data
    @NotEmpty
    private List<ReceiverData> receiverDataList;

    // mesage data
    @NotNull
    private MessageBodyType messageBodyType;
    private JsonNode messageBody;
}
