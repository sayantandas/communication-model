package com.bricknbolt.garuda.communicationModel.Request.Whatsapp;

import com.bricknbolt.garuda.communicationModel.Request.SenderMessageRequest;

public interface WhatsappMessageRequest extends SenderMessageRequest {
}
