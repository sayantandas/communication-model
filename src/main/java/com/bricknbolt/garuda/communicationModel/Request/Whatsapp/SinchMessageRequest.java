package com.bricknbolt.garuda.communicationModel.Request.Whatsapp;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
// post this object to send whatsapp message via sinch, sinch consumes it
public class SinchMessageRequest implements WhatsappMessageRequest {
    List<String> to;
    MessageBody message;
}
