package com.bricknbolt.garuda.communicationModel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommunicationModelApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommunicationModelApplication.class, args);
    }

}
