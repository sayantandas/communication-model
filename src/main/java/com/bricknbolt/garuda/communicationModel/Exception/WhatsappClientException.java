package com.bricknbolt.garuda.communicationModel.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class WhatsappClientException extends Exception {
    public WhatsappClientException(String message) {
        super(message);
    }
}
