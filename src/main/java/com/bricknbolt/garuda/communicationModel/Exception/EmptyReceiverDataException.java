package com.bricknbolt.garuda.communicationModel.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EmptyReceiverDataException extends Exception {
    public EmptyReceiverDataException(String ex){
        super(ex);
    }
}
