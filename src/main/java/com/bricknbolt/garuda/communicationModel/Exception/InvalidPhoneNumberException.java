package com.bricknbolt.garuda.communicationModel.Exception;

public class InvalidPhoneNumberException extends Exception {
    public InvalidPhoneNumberException(String ex){
        super(ex);
    }
}
