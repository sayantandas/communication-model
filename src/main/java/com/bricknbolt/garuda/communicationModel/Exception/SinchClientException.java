package com.bricknbolt.garuda.communicationModel.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class SinchClientException extends WhatsappClientException {
    public SinchClientException(String message) {
        super(message);
    }
}
