package com.bricknbolt.garuda.communicationModel.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundInDbException extends Exception {
    public NotFoundInDbException(String ex){
        super(ex);
    }
}
