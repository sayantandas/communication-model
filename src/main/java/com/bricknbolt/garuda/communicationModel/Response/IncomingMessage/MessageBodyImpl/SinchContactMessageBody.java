package com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.Contact.Contact;
import com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.ContactMessageBody;
import com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.SinchMessageBody;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class SinchContactMessageBody implements SinchMessageBody {
    private String type;
    private List<Contact> contacts;

    @Override
    public MessageBody convertSinchMessageBodyToMessageBody(){
        ContactMessageBody messageMetaData = new ContactMessageBody();
        messageMetaData.setContacts(contacts);
        return messageMetaData;
    }
}
