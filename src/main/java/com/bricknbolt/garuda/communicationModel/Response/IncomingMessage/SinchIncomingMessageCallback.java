package com.bricknbolt.garuda.communicationModel.Response.IncomingMessage;

import com.fasterxml.jackson.databind.JsonNode;
import java.sql.Timestamp;

public class SinchIncomingMessageCallback implements WhatsappIncomingMessageCallback {
    private String from;
    private String in_group;
    private String to; // value = sinchbot
    private Object replying_to; //TODO : adding this message to conversation
    private String message_id;
    private JsonNode message;
    private Object timestamp;

    @Override
    public String getFrom() {
        return from;
    }

    @Override
    public String getIn_group() {
        return in_group;
    }

    @Override
    public String getTo() {
        return to;
    }

    @Override
    public Object getReplying_to() {
        return replying_to;
    }

    @Override
    public String getMessage_id() {
        return message_id;
    }

    @Override
    public JsonNode getMessage() {
        return message;
    }

    @Override
    public Timestamp getTimestamp() {
        return (Timestamp)timestamp;
    }

    @Override
    public String toString() {
        return "SinchIncomingMessageCallback{" +
                "from='" + from + '\'' +
                ", in_group='" + in_group + '\'' +
                ", to='" + to + '\'' +
                ", replying_to=" + replying_to +
                ", message_id='" + message_id + '\'' +
                ", message=" + message +
                ", timestamp=" + timestamp +
                '}';
    }
}
