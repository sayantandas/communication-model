package com.bricknbolt.garuda.communicationModel.Response.OutgoingMessage;

import java.util.Date;

public class SinchDeliveryReportCallback implements WhatsappDeliveryReportCallback {
    private String status;
    private SinchState state;
    private String message_id;
    private String details;
    private String recipient;
    private Object timestamp;

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public SinchState getState() {
        return state;
    }

    @Override
    public String getMessage_id() {
        return message_id;
    }

    @Override
    public String getDetails() {
        return details;
    }

    @Override
    public String getRecipient() {
        return recipient;
    }

    @Override
    public Date getTimestamp() {
        return (Date)timestamp;
    }

    @Override
    public String toString() {
        return "SinchDeliveryReportCallback{" +
                "status='" + status + '\'' +
                ", state=" + state +
                ", message_id='" + message_id + '\'' +
                ", details='" + details + '\'' +
                ", recipient='" + recipient + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
