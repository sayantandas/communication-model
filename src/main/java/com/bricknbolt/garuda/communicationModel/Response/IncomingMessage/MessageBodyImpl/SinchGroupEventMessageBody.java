package com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.SinchMessageBody;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class SinchGroupEventMessageBody implements SinchMessageBody {
    private String type;
    private String body;
    private List<String> members;
    private GroupEventType event_type;
    private String in_group;

    @Override
    public MessageBody convertSinchMessageBodyToMessageBody() {
        return null;
    }
}
