package com.bricknbolt.garuda.communicationModel.Response.IncomingMessage;

import lombok.Data;

@Data
public class SinchMessage {
    private String type;
    private SinchMessageBody sinchMessageBody;
}
