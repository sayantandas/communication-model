package com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.MessageBodyImpl;

public enum GroupEventType {
    group_user_joined, group_user_left;
}
