package com.bricknbolt.garuda.communicationModel.Response.IncomingMessage;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;

public interface SinchMessageBody {
    MessageBody convertSinchMessageBodyToMessageBody();
}

