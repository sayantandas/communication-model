package com.bricknbolt.garuda.communicationModel.Response;

import com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.SinchIncomingMessageCallback;
import com.bricknbolt.garuda.communicationModel.Response.OutgoingMessage.SinchDeliveryReportCallback;

import java.util.List;

//sinch produces this
public class SinchCallback implements WhatsappCallback {
    private String type;
    // callback for outgoing messages
    private List<SinchDeliveryReportCallback> statuses;
    // callback for inbound messages
    private List<SinchIncomingMessageCallback> notifications;

    // present in case of http error 400 or 401
    private String message;

    @Override
    public String toString() {
        return "SinchCallback{" +
                "type='" + type + '\'' +
                ", statuses=" + statuses +
                ", notifications=" + notifications +
                ", message='" + message + '\'' +
                ", details='" + details + '\'' +
                '}';
    }

    private String details;

    @Override
    public String getType() {
        return type;
    }

    @Override
    public List<SinchDeliveryReportCallback> getStatuses() {
        return statuses;
    }

    @Override
    public List<SinchIncomingMessageCallback> getNotifications() {
        return notifications;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDetails() {
        return details;
    }
}
