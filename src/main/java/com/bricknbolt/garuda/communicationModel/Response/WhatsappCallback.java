package com.bricknbolt.garuda.communicationModel.Response;

import com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.SinchIncomingMessageCallback;
import com.bricknbolt.garuda.communicationModel.Response.OutgoingMessage.SinchDeliveryReportCallback;

import java.util.List;

public interface WhatsappCallback {
    String getType();

    List<SinchDeliveryReportCallback> getStatuses();

    List<SinchIncomingMessageCallback> getNotifications();

    String getMessage();

    String getDetails();
}
