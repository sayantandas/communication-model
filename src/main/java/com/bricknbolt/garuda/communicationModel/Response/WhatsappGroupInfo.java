package com.bricknbolt.garuda.communicationModel.Response;

import java.util.List;

public interface WhatsappGroupInfo {
    List<String> getAdmins();

    String getCreation_time();

    String getCreator();

    List<String> getMembers();

    String getSubject();
}
