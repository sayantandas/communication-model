package com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.*;
import com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.SinchMessageBody;
import com.bricknbolt.garuda.communicationModel.Util.MessageBodyFactory;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SinchMediaMessageBody implements SinchMessageBody {
    private String type;
    private String url;
    private String mime_type;
    private String caption;
    private String filename;

    @Override
    public MessageBody convertSinchMessageBodyToMessageBody(){
        MessageBody messageBody = null;

        if(type.equalsIgnoreCase(String.valueOf(MessageBodyType.video))){
            messageBody = MessageBodyFactory.getMessageBody(MessageBodyType.video);
            ((VideoMessageBody) messageBody).setUrl(url);
            ((VideoMessageBody) messageBody).setCaption(caption);
        }
        else if(type.equalsIgnoreCase(String.valueOf(MessageBodyType.image))){
            messageBody = MessageBodyFactory.getMessageBody(MessageBodyType.image);
            ((ImageMessageBody) messageBody).setUrl(url);
            ((ImageMessageBody) messageBody).setCaption(caption);
        }
        else if(type.equalsIgnoreCase(String.valueOf(MessageBodyType.document))){
            messageBody = MessageBodyFactory.getMessageBody(MessageBodyType.document);
            ((DocumentMessageBody) messageBody).setUrl(url);
            ((DocumentMessageBody) messageBody).setCaption(caption);
        }
        else if(type.equalsIgnoreCase(String.valueOf(MessageBodyType.audio))){
            messageBody = MessageBodyFactory.getMessageBody(MessageBodyType.audio);
            ((AudioMessageBody) messageBody).setUrl(url);
        }
        return messageBody;
    }
}
