package com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.LocationMessageBody;
import com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.SinchMessageBody;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SinchLocationMessageBody implements SinchMessageBody {
    private String type;
    private Double lat;
    private Double lng;
    private String address;
    private String name;
    private String url;

    @Override
    public MessageBody convertSinchMessageBodyToMessageBody(){
        LocationMessageBody messageMetaData = new LocationMessageBody();
        messageMetaData.setLat(lat);
        messageMetaData.setLng(lng);
        messageMetaData.setName(name);
        messageMetaData.setAddress(address);
        return messageMetaData;
    }
}
