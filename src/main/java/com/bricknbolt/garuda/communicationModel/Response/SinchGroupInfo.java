package com.bricknbolt.garuda.communicationModel.Response;

import java.util.List;

//sinch produces this
public class SinchGroupInfo implements WhatsappGroupInfo {
    private List<String> admins;
    private String creation_time;
    private String creator;
    private List<String> members;
    private String subject;

    public List<String> getAdmins() {
        return admins;
    }

    public String getCreation_time() {
        return creation_time;
    }

    public String getCreator() {
        return creator;
    }

    public List<String> getMembers() {
        return members;
    }

    public String getSubject() {
        return subject;
    }
}
