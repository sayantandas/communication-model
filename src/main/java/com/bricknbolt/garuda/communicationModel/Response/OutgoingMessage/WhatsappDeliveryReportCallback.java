package com.bricknbolt.garuda.communicationModel.Response.OutgoingMessage;

import java.util.Date;

public interface WhatsappDeliveryReportCallback {
    String getStatus();

    SinchState getState();

    String getMessage_id();

    String getDetails();

    String getRecipient();

    Date getTimestamp();
}
