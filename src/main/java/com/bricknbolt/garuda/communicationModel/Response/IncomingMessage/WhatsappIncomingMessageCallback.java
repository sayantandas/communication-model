package com.bricknbolt.garuda.communicationModel.Response.IncomingMessage;

import com.fasterxml.jackson.databind.JsonNode;

import java.sql.Timestamp;

public interface WhatsappIncomingMessageCallback {
    String getFrom();

    String getIn_group();

    String getTo();

    Object getReplying_to();

    String getMessage_id();

    JsonNode getMessage();

    Timestamp getTimestamp();
}
