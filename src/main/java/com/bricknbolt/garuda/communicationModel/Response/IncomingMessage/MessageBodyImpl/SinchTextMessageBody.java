package com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.TextMessageBody;
import com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.SinchMessageBody;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SinchTextMessageBody implements SinchMessageBody {
    private String type;
    private String body;

    @Override
    public MessageBody convertSinchMessageBodyToMessageBody(){
        TextMessageBody messageMetaData = new TextMessageBody();
        messageMetaData.setText(body);
        return messageMetaData;
    }
}
