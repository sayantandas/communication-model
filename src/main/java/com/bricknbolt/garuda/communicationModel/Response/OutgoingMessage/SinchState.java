package com.bricknbolt.garuda.communicationModel.Response.OutgoingMessage;

public enum SinchState {
    queued	("Message has been received and queued by the Sinch WhatsApp API"),
    dispatched	("Message has been dispatched by Sinch WhatsApp API to WhatsApp servers"),
    sent	("Message has been sent by WhatsApp to end-user"),
    delivered	("Message has been successfully delivered to end-user by WhatsApp"),
    read	("Message has been read by the end-user in the WhatsApp application"),
    deleted	("Message has been deleted or expired in the application"),
    failed	("Message has failed"),
    no_opt_in	("Message rejected by Sinch API as recipient is not registered to have opted in"),
    no_capability	("Message rejected by the Sinch API as the recipient lacks WhatsApp capability");

    private String value;
    SinchState(String value) {
        this.value = value;
    }
}
