package com.bricknbolt.garuda.communicationModel.Util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

public class CommunicationModelUtil {

    private static String getURIString(String url, MultiValueMap<String,String> queryParams){
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
        if(!queryParams.isEmpty()){
            builder = builder.queryParams(queryParams);
        }
        return builder.toUriString();
    }

    public static String getURIStringWithoutParams(String url){
        return getURIString(url, new HttpHeaders()); // to generate empty multivalue map impl
    }

    public static String getURIStringWithParams(String url, MultiValueMap<String,String> queryParams){
        return getURIString(url, queryParams);
    }

    public static HttpHeaders getHttpHeaders(String token){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        if(!StringUtils.isEmpty(token))
            headers.set("Authorization", "Bearer " + token);
        return headers;
    }
}
