package com.bricknbolt.garuda.communicationModel.Util;

import com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.*;
import com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.MessageBodyImpl.*;
import com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.SinchMessageBody;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.StringUtils;

import java.io.IOException;

public class SinchMessageBodyFactory {
    public static SinchMessageBody getMessageBodyFromJSON(MessageBodyType messageBodyType, JsonNode json) throws IOException {

        final ObjectMapper objectMapper = new ObjectMapper();
        String jsonStr = objectMapper.writeValueAsString(json);
        if(StringUtils.isEmpty(jsonStr))
            return null;

        SinchMessageBody messageBody;
        if(messageBodyType.equals(MessageBodyType.audio) || messageBodyType.equals(MessageBodyType.document) ||
                messageBodyType.equals(MessageBodyType.image) || messageBodyType.equals(MessageBodyType.video) ||
                messageBodyType.equals(MessageBodyType.voice)){
            messageBody = objectMapper.readValue(jsonStr, SinchMediaMessageBody.class);
        }
        else if(messageBodyType.equals(MessageBodyType.contacts)){
            messageBody = objectMapper.readValue(jsonStr, SinchContactMessageBody.class);
        }
        else if(messageBodyType.equals(MessageBodyType.location)){
            messageBody = objectMapper.readValue(jsonStr, SinchLocationMessageBody.class);
        }
        else if(messageBodyType.equals(MessageBodyType.text)){
            messageBody = objectMapper.readValue(jsonStr, SinchTextMessageBody.class);
        }
        else if(messageBodyType.equals(MessageBodyType.group_event)){
            messageBody = objectMapper.readValue(jsonStr, SinchGroupEventMessageBody.class);
        }
        else {
            messageBody = null;
        }
        return messageBody;
    }
}

