package com.bricknbolt.garuda.communicationModel.Util;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.StringUtils;

import java.io.IOException;

//Factory Pattern to get Message based on messageType
// contains both noArgs and copy constructor
public class MessageBodyFactory {
    private static MessageBody getMessageBodyUtil(MessageBodyType messageBodyType, MessageBody body){
        MessageBody messageBody;
        if(messageBodyType.equals(MessageBodyType.audio)){
            if(null == body)
                messageBody = new AudioMessageBody();
            else
                messageBody = new AudioMessageBody((AudioMessageBody)body);
        }
        else if(messageBodyType.equals(MessageBodyType.contacts)){
            if(null == body)
                messageBody = new ContactMessageBody();
            else
                messageBody = new ContactMessageBody((ContactMessageBody) body);
        }
        else if(messageBodyType.equals(MessageBodyType.document)){
            if(null == body)
                messageBody = new DocumentMessageBody();
            else
                messageBody = new DocumentMessageBody((DocumentMessageBody) body);
        }
        else if(messageBodyType.equals(MessageBodyType.image)){
            if(null == body)
                messageBody = new ImageMessageBody();
            else
                messageBody = new ImageMessageBody((ImageMessageBody) body);
        }
        else if(messageBodyType.equals(MessageBodyType.location)){
            if(null == body)
                messageBody = new LocationMessageBody();
            else
                messageBody = new LocationMessageBody((LocationMessageBody) body);
        }
        else if(messageBodyType.equals(MessageBodyType.template)){
            if(null == body)
                messageBody = new TemplateMessageBody();
            else
                messageBody = new TemplateMessageBody((TemplateMessageBody) body);
        }
        else if(messageBodyType.equals(MessageBodyType.text)){
            if(null == body)
                messageBody = new TextMessageBody();
            else
                messageBody = new TextMessageBody((TextMessageBody) body);
        }
        else if(messageBodyType.equals(MessageBodyType.video)){
            if(null == body)
                messageBody = new VideoMessageBody();
            else
                messageBody = new VideoMessageBody((VideoMessageBody) body);
        }
        else if(messageBodyType.equals(MessageBodyType.group_event)){
            if(null == body)
                messageBody = new GroupEventMessageBody();
            else
                messageBody = new GroupEventMessageBody((GroupEventMessageBody) body);
        }
        else {
            messageBody = null;
        }
        return messageBody;
    }
    public static MessageBody getMessageBody(MessageBodyType messageBodyType, MessageBody body){
        return getMessageBodyUtil(messageBodyType, body);
    }
    public static MessageBody getMessageBody(MessageBodyType messageBodyType){
        return getMessageBodyUtil(messageBodyType, null);
    }

    public static MessageBody getMessageBodyFromJSON(MessageBodyType messageBodyType, JsonNode json) throws IOException {

        final ObjectMapper objectMapper = new ObjectMapper();
        String jsonStr = objectMapper.writeValueAsString(json);
        if(StringUtils.isEmpty(jsonStr))
            return null;

        MessageBody messageBody;
        if(messageBodyType.equals(MessageBodyType.audio)){
            messageBody = objectMapper.readValue(jsonStr, AudioMessageBody.class);
        }
        else if(messageBodyType.equals(MessageBodyType.contacts)){
            messageBody = objectMapper.readValue(jsonStr, ContactMessageBody.class);
        }
        else if(messageBodyType.equals(MessageBodyType.document)){
            messageBody = objectMapper.readValue(jsonStr, DocumentMessageBody.class);
        }
        else if(messageBodyType.equals(MessageBodyType.image)){
            messageBody = objectMapper.readValue(jsonStr, ImageMessageBody.class);
        }
        else if(messageBodyType.equals(MessageBodyType.location)){
            messageBody = objectMapper.readValue(jsonStr, LocationMessageBody.class);
        }
        else if(messageBodyType.equals(MessageBodyType.template)){
            messageBody = objectMapper.readValue(jsonStr, TemplateMessageBody.class);
        }
        else if(messageBodyType.equals(MessageBodyType.text)){
            messageBody = objectMapper.readValue(jsonStr, TextMessageBody.class);
        }
        else if(messageBodyType.equals(MessageBodyType.video)){
            messageBody = objectMapper.readValue(jsonStr, VideoMessageBody.class);
        }
        else if(messageBodyType.equals(MessageBodyType.group_event)){
            messageBody = objectMapper.readValue(jsonStr, GroupEventMessageBody.class);
        }
        else {
            messageBody = null;
        }
        return messageBody;
    }
}
