package com.bricknbolt.garuda.communicationModel.Model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

//serviceRequestId and conversationType uniquely determine Conversation
@Data
@Document("Conversations")
@NoArgsConstructor
public class Conversation {
    @Id
    private String id;

    // set from message request
    @NotNull
    private String serviceRequestId;
    @NotNull
    private String providerConversationId; // either mobile number or groupId

    @NotNull
    private ConversationType conversationType;
    private ConversationState conversationState;
    private List<String> messageIdList;
    @NotNull
    private Date createdDate;
    private Date updatedDate;
}
