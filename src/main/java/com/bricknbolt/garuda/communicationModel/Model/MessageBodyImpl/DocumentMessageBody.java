package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl;


import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DocumentMessageBody implements MessageBody {
    private MessageBodyType type = MessageBodyType.document;
    private String url;
    private String caption;
    private String filename;

    public MessageBodyType getType(){ return type; }
    public DocumentMessageBody(DocumentMessageBody documentMessageBody){
        this.url = documentMessageBody.getUrl();
        this.caption = documentMessageBody.getCaption();
        this.filename = documentMessageBody.getFilename();
    }
}

