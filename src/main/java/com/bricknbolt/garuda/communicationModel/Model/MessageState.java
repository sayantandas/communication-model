package com.bricknbolt.garuda.communicationModel.Model;

public enum MessageState {
    SUCCESS("success"), PROVIDER_FAILURE("provider failed to deliver message"), HTTP_FAILURE("call to provider failed");
    private String value;

    MessageState(String value) {
        this.value = value;
    }
}
