package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.Contact;

public class ContactMobile {
    private String type;
    private String phone;

    public String getType() {
        return type;
    }

    public String getPhone() {
        return phone;
    }
}
