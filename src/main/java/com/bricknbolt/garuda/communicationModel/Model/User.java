package com.bricknbolt.garuda.communicationModel.Model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Document("Users")
@NoArgsConstructor
public class User {
    @Id
    private String id;
    private String name;
    @NotNull
    private String mobile;
    @NotNull
    private Date createdDate;
    private Date updatedDate;
    private boolean isActive;
}
