package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.Contact;

public class ContactURL {
    private String type;
    private String url;

    public String getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }
}
