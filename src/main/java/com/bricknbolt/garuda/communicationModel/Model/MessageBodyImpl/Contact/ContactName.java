package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.Contact;

public class ContactName {
    private String formatted_name;
    private String first_name;
    private String last_name;
    private String middle_name;
    private String suffix;
    private String prefix;

    public String getFormatted_name() {
        return formatted_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getPrefix() {
        return prefix;
    }
}
