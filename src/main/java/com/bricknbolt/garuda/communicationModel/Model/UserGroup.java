package com.bricknbolt.garuda.communicationModel.Model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Document("UserGroups")
@NoArgsConstructor
public class UserGroup {
    @Id
    private String id;
    @NotNull
    private String userId;
    @NotNull
    private String groupId;
    @NotNull
    private Date createdDate;
    private Date updatedDate;
    private boolean isActive;
}