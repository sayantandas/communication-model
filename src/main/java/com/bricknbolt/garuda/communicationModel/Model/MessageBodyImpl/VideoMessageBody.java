package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VideoMessageBody implements MessageBody {
    private MessageBodyType type = MessageBodyType.video;
    private String url;
    private String caption;

    public MessageBodyType getType(){ return type; }
    public VideoMessageBody(VideoMessageBody videoMessageBody) {
        this.url = videoMessageBody.getUrl();
        this.caption = videoMessageBody.getCaption();
    }
}