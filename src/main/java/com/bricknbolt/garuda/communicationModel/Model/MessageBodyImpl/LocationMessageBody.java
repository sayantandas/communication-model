package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LocationMessageBody implements MessageBody {
    private MessageBodyType type = MessageBodyType.location;
    private Double lat;
    private Double lng;
    private String name;
    private String address;

    public MessageBodyType getType(){ return type; }
    public LocationMessageBody(LocationMessageBody locationMessageBody) {
        this.lat = locationMessageBody.getLat();
        this.lng = locationMessageBody.getLng();
        this.name = locationMessageBody.getName();
        this.address = locationMessageBody.getAddress();
    }
}
