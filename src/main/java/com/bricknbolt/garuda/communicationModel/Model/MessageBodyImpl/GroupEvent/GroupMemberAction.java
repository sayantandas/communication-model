package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.GroupEvent;

public enum GroupMemberAction {
    group_user_joined, group_user_left;
}
