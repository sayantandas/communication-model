package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl;

public enum MessageBodyType {
    template, text, image, video, document, audio, location, voice, contacts, group_event;
}
