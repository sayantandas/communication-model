package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TextMessageBody implements MessageBody {
    private MessageBodyType type = MessageBodyType.text;
    private boolean preview_url;
    private String text;

    public MessageBodyType getType(){ return type; }
    public TextMessageBody(TextMessageBody textMessageBody) {
        this.preview_url = textMessageBody.isPreview_url();
        this.text = textMessageBody.getText();
    }
}
