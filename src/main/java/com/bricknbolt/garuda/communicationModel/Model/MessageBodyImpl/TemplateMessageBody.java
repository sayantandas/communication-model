package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class TemplateMessageBody implements MessageBody {
    private MessageBodyType type = MessageBodyType.template;
    private String template_name;
    private String language;
    private List<String> params;
    private String ttl;

    public MessageBodyType getType(){ return type; }
    public TemplateMessageBody(TemplateMessageBody templateMessageBody) {
        this.template_name = templateMessageBody.getTemplate_name();
        this.language = templateMessageBody.getLanguage();
        this.params = templateMessageBody.getParams();
        this.ttl = templateMessageBody.getTtl();
    }
}
