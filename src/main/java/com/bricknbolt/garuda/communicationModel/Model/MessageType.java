package com.bricknbolt.garuda.communicationModel.Model;

public enum MessageType {
    outgoing, incoming;
}
