package com.bricknbolt.garuda.communicationModel.Model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Data
@Document("UsedIds")
@NoArgsConstructor
public class UsedIds {
    @Id
    private String id;
    @NotNull
    private String collectionName;
    @NotNull
    private String generatedId;
}
