package com.bricknbolt.garuda.communicationModel.Model;

public enum ConversationState {
    active("this conversationId is active"),
    closed("conversationId marked as closed"),
    inactive("conversationId is inactive because some other conversationId is active for that serviceRequestId");

    private String value;
    ConversationState(String value){
        this.value = value;
    }
}
