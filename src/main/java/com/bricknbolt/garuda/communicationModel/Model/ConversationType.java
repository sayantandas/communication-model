package com.bricknbolt.garuda.communicationModel.Model;

public enum ConversationType {
    direct, group;
}
