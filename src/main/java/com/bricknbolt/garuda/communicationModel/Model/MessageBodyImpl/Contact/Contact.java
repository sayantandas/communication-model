package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.Contact;

import java.util.List;

public class Contact {
    private List<ContactAddress> addresses;
    private String birthday;
    private List<ContactEmail> emails;
    private List<ContactIM> ims;
    private ContactName name;
    private ContactOrg org;
    private List<ContactMobile> phones;
    private List<ContactURL> urls;

    public List<ContactAddress> getAddresses() {
        return addresses;
    }

    public String getBirthday() {
        return birthday;
    }

    public List<ContactEmail> getEmails() {
        return emails;
    }

    public List<ContactIM> getIms() {
        return ims;
    }

    public ContactName getName() {
        return name;
    }

    public ContactOrg getOrg() {
        return org;
    }

    public List<ContactMobile> getPhones() {
        return phones;
    }

    public List<ContactURL> getUrls() {
        return urls;
    }
}
