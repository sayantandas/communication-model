package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ImageMessageBody implements MessageBody {
    private MessageBodyType type = MessageBodyType.image;
    private String url;
    private String caption;

    public MessageBodyType getType(){ return type; }
    public ImageMessageBody(ImageMessageBody imageMessageBody) {
        this.url = imageMessageBody.getUrl();
        this.caption = imageMessageBody.getCaption();
    }
}
