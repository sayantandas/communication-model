
package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AudioMessageBody implements MessageBody {
    private MessageBodyType type = MessageBodyType.audio;
    private String url;

    public MessageBodyType getType(){ return type; }
    public AudioMessageBody(AudioMessageBody audioMessageBody){
        this.url = audioMessageBody.getUrl();
    }
}

