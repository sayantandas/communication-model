package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import com.bricknbolt.garuda.communicationModel.Response.IncomingMessage.MessageBodyImpl.GroupEventType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
public class GroupEventMessageBody implements MessageBody {
    private MessageBodyType type = MessageBodyType.group_event;
    private String groupId;
    private GroupEventType groupEventType;
    private String memberId;
    private List<String> members;

    public MessageBodyType getType(){ return type; }
    public GroupEventMessageBody(GroupEventMessageBody groupEventMessageBody) {
        this.groupId = groupEventMessageBody.getGroupId();
        this.groupEventType = groupEventMessageBody.getGroupEventType();
        this.memberId = groupEventMessageBody.getMemberId();
        Collections.copy(this.members, groupEventMessageBody.getMembers());
    }
}
