package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl;

import com.bricknbolt.garuda.communicationModel.Model.MessageBody;
import com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.Contact.Contact;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
public class ContactMessageBody implements MessageBody {
    private MessageBodyType type = MessageBodyType.contacts;
    private List<Contact> contacts;

    public MessageBodyType getType(){ return type; }
    public ContactMessageBody(ContactMessageBody contactMessageBody){
        Collections.copy(this.contacts, contactMessageBody.getContacts());
    }
}
