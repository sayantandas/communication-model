package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.Contact;

public class ContactOrg {
    private String company;
    private String department;
    private String title;

    public String getCompany() {
        return company;
    }

    public String getDepartment() {
        return department;
    }

    public String getTitle() {
        return title;
    }
}
