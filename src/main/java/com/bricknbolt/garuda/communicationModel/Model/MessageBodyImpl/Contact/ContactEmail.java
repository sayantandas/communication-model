package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.Contact;

public class ContactEmail {
    private String type;
    private String email;

    public String getType() {
        return type;
    }

    public String getEmail() {
        return email;
    }
}
