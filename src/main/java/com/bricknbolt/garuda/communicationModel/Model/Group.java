package com.bricknbolt.garuda.communicationModel.Model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@Document("Groups")
@NoArgsConstructor
public class Group {
    @Id
    private String id;
    @NotNull
    private String providerId;
    @NotNull
    private String creatorId;
    private List<String> admins;
    private List<String> members;
    private Date createdDate;
    private Date updatedDate;
    private String subject;
    private boolean isActive;
}
