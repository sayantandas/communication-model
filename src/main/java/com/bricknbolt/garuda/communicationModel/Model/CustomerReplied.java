package com.bricknbolt.garuda.communicationModel.Model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

@Data
@Document("CustomerReplied")
@NoArgsConstructor
public class CustomerReplied {
    @Id
    private String id;
    @NotNull
    private String serviceRequestId;
    private Date createdDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomerReplied)) return false;
        CustomerReplied that = (CustomerReplied) o;
        return getServiceRequestId().equals(that.getServiceRequestId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getServiceRequestId());
    }
}
