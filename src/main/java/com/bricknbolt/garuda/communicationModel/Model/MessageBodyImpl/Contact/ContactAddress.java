package com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.Contact;

public class ContactAddress {
    private String type;
    private String street;
    private String city;
    private String state;
    private String zip;
    private String country;
    private String country_code;

    public String getType() {
        return type;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public String getCountry() {
        return country;
    }

    public String getCountry_code() {
        return country_code;
    }
}

