package com.bricknbolt.garuda.communicationModel.Model;

import com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.MessageBodyType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Document("Messages")
@NoArgsConstructor
public class Message {
    @Id
    private String id; // primary key
    @NotNull
    private String conversationId;
    @NotNull
    private String senderMobile;

    @NotNull
    private MessageType messageType;
    @NotNull
    private MessageBodyType messageBodyType;
    private MessageBody messageBody;

    @NotNull
    private MessageState messageState; // set after call to whatsapp client

    private String providerState;
    private String providerDetails;
    private String providerMessageId;

    @NotNull
    private Date createdDate;
    private Date updatedDate;
}
