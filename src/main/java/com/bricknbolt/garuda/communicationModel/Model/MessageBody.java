package com.bricknbolt.garuda.communicationModel.Model;

import com.bricknbolt.garuda.communicationModel.Model.MessageBodyImpl.MessageBodyType;

public interface MessageBody {
    MessageBodyType getType();
}
